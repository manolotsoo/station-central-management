<?php

use App\Models\StockModel;
use \Config\Database;

function get_quantity_out_per_product()
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('_name,sales.product_id, SUM(quantity) as quantity_out')
    ->join('products', 'sales.product_id=products.id')
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function get_entry()
{
  $db      = Database::connect();
  $builder = $db->table('stocks');
  $builder->select('_name,product_id, SUM(quantity_entry) as quantity_entry')
    ->join('products', 'stocks.product_id=products.id')
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function get_state_stocks()
{
  $db      = Database::connect();
  $builder = $db->table('stocks');
  $builder->select('_name, product_id, sum(quantity_entry)-sum(quantity_out) as stock_state')
    ->join('products', 'stocks.product_id=products.id')
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
  
}
