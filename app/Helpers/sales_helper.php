<?php

use \Config\Database;

function sales_group_by_stations()
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('station_id,_name, address, sum(quantity) as quantity, sum(total_price) as price')
    ->join("stations", "sales.station_id=stations.id")
    ->groupBy('station_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_stations_per_date($station_id)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('date(created_at) as date, sum(quantity), sum(total_price) as price')
    ->where("station_id", $station_id)
    ->groupBy('date(created_at)');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_stations($station_id)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('station_id,date(sales.created_at) as created_at, sum(quantity), sum(total_price) as price')
    ->join("stations", "sales.station_id=stations.id")
    ->where('station_id', $station_id)
    ->groupBy('date(created_at)');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}


function sales_per_day_per_station($station_id)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('station_id,date(sales.created_at) as created_at, sum(quantity), sum(total_price) as price')
    ->join("stations", "sales.station_id=stations.id")
    ->where('station_id', $station_id)
    ->groupBy('date(created_at)');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_date_per_station($station_id, $date)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('product_id,_name, quantity, total_price, station_id')
    ->join("products", "products.id=sales.product_id")
    ->where(['station_id' => $station_id, 'date(sales.created_at)' => $date]);
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_station($station_id)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('product_id, SUM(quantity) as quantity, SUM(total_price) as total_price, station_id')
    ->where('station_id', $station_id)
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_station_per_date($station_id, $date)
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('product_id, SUM(quantity) as quantity , SUM(total_price) as total_price')
    ->where(['station_id' => $station_id, 'DATE(created_at)' => $date])
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function sales_per_product()
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('product_id, _name, SUM(selling_price) - SUM(cost_price)  as benefit, SUM(quantity) as quantity , SUM(total_price) as total_price')
    ->join("products", "sales.product_id=products.id")
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function get_label_per_product()
{
  $res = array();
  $temp = sales_per_product();
  foreach ($temp as $res_temp) {
    array_push($res, $res_temp['_name']);
  }
  return $res;
}

function get_value_per_product()
{
  $res = array();
  $temp = sales_per_product();
  foreach ($temp as $res_temp) {
    array_push($res, $res_temp['total_price']);
  }
  return $res;
}

function get_benefit()
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('product_id, _name, SUM(selling_price), cost_price,cost_price * SUM(quantity) as revient, SUM(quantity) as quantity , (SUM(total_price) - cost_price * SUM(quantity)) as benefit')
    ->join("products", "sales.product_id=products.id")
    ->groupBy('product_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function get_sales_per_station()
{
  $db      = Database::connect();
  $builder = $db->table('sales');
  $builder->select('station_id, stations._name as _name,SUM(cost_price) * SUM(quantity) as revient, SUM(quantity) as quantity, SUM(total_price) as sales_per_station , (SUM(total_price) - SUM(cost_price) * SUM(quantity)) as benefit')
    ->join("products", "sales.product_id=products.id")
    ->join("stations", "sales.station_id=stations.id")
    ->groupBy('station_id');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}
