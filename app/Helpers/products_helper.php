<?php

use App\Models\ProductModel;
use \Config\Database;

function allProduct()
{
  $db      = Database::connect();
  $builder = $db->table('products');
  $builder->select('id, _name, cost_price, selling_price, evaporation_percent');
  $query = $builder->get();
  $db->close();
  return $query->getResultArray();
}

function getProductById($id)
{
  $productModel = new ProductModel();
  return $productModel->find($id);
}
