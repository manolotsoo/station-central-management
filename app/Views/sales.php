<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ventes - liste</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">


    <!-- top navbar -->
    <?= View("components/navbar") ?>

    <!-- the left sidebar -->
    <?= View("components/main-sidebar-container", ['titleLocation' => $titleLocation]) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <?= View("components/content-header.php", ["breadCrumb" => $breadCrumb, "titleLocation" => $titleLocation]) ?>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-3 mt-2">
                    <div class="card card-danger">
                      <div class="card-header">
                        <h3 class="card-title">Statistique</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                          </button>
                          <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                          </button>
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="chartjs-size-monitor">
                          <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                          </div>
                          <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                          </div>
                        </div>
                        <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 776px;" width="545" height="175" class="chartjs-render-monitor"></canvas>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-12">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">Benefice par produit</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body p-0">
                        <table class="table table-sm">
                          <thead>
                            <tr>
                              <th>Libellé produit</th>
                              <th>Prix</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($benefits as $benefit) { ?>
                              <tr>
                                <td><?= $benefit['_name'] ?></td>
                                <td><?= $benefit['benefit'] ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer ml-auto" style="margin-right: 66px;">
                        total : <?= $benefit_total ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-12">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">Vente par station</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body p-0">
                        <table class="table table-sm">
                          <thead>
                            <tr>
                              <th>Station</th>
                              <th>Prix</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($stations_stats as $station) { ?>
                              <tr>
                                <td><?= $station['_name'] ?></td>
                                <td><?= $station['sales_per_station'] ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer ml-auto" style="margin-right: 108px;">
                        total : <?= $sale_total ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <form action="<?= base_url() ?>/sales/import_csv" method="POST" enctype="multipart/form-data">
                      <input type="hidden" name="location" value="<?= $titleLocation ?>">
                      <div class="form-group">
                        <label for="exampleInputFile">Importer un fichier de vente journalier</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" name="file_to_upload" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choisir un fichier</label>
                          </div>
                        </div>
                      </div>
                      <button type="submit" name="import" class="btn btn-block btn-info"> <i class="fa fa-file"></i> Importer</button>
                    </form>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="data_table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Product id</th>
                      <!-- <th>Responsable</th> -->
                      <th>Quantity</th>
                      <th>Total price</th>
                      <th>Updated at</th>
                      <th>Created at</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- Add modal content -->
      <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="text-center bg-info p-3">
              <h4 class="modal-title text-white" id="info-header-modalLabel">Add</h4>
            </div>
            <div class="modal-body">
              <form id="add-form" class="pl-3 pr-3" action="<?= base_url() ?>/sales/create" method="POST">
                <div class="row">
                  <input type="hidden" id="id" name="id" class="form-control" placeholder="Id" required>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="productId"> Product id: <span class="text-danger">*</span> </label>
                      <select class="form-control" name="productId" id="">
                        <?php foreach ($products as $product) { ?>
                          <option value="<?= $product['id'] ?>"><?= $product['_name'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <!-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="responsable"> Responsable: <span class="text-danger">*</span> </label>
                      <input type="number" id="responsable" name="responsable" class="form-control" placeholder="Responsable" number="true" required>
                    </div>
                  </div> -->
                  <div class="col-md-12">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="sales_type" value="1">
                      <!-- <label class="form-check-label">Prix</label> -->
                      <!-- input -->
                      <div class="form-group">
                        <label for="totalPrice"> Total price: <span class="text-danger">*</span> </label>
                        <input type="text" id="totalPrice" name="totalPrice" class="form-control" placeholder="Total price">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="sales_type" value="0">
                      <!-- <label class="form-check-label">Quantité</label> -->
                      <!-- input -->
                      <div class="form-group">
                        <label for="quantity"> Quantity: <span class="text-danger">*</span> </label>
                        <input type="text" id="quantity" name="quantity" class="form-control" placeholder="Quantity">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="totalPrice"> Total price: <span class="text-danger">*</span> </label>
                      <input type="text" id="totalPrice" name="totalPrice" class="form-control" placeholder="Total price" required>
                    </div>
                  </div> -->
                  <!-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="updatedAt"> Updated at: </label>
                      <input type="text" id="updatedAt" name="updatedAt" class="form-control" placeholder="Updated at">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="createdAt"> Created at: </label>
                      <input type="text" id="createdAt" name="createdAt" class="form-control" placeholder="Created at">
                    </div>
                  </div> -->
                </div>
                <div class="row">
                </div>

                <div class="form-group text-center">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success" id="add-form-btn">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- Add modal content -->
      <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="text-center bg-info p-3">
              <h4 class="modal-title text-white" id="info-header-modalLabel">Update</h4>
            </div>
            <div class="modal-body">
              <form id="edit-form" class="pl-3 pr-3">
                <div class="row">
                  <input type="hidden" id="id" name="id" class="form-control" placeholder="Id" required>
                </div>
                <div class="row">
                  <!-- replace with select input -->
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="productId"> Product id: <span class="text-danger">*</span> </label>
                      <select class="form-control" name="productId" id="">
                        <?php foreach ($products as $product) { ?>
                          <option value="<?= $product['id'] ?>"><?= $product['_name'] ?></option>
                        <?php } ?>
                      </select>
                      <!-- <input type="number" id="productId" name="productId" class="form-control" placeholder="Product id" number="true" required> -->
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="responsable"> Responsable: <span class="text-danger">*</span> </label>
                      <input type="number" id="responsable" name="responsable" class="form-control" placeholder="Responsable" number="true" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="quantity"> Quantity: <span class="text-danger">*</span> </label>
                      <input type="text" id="quantity" name="quantity" class="form-control" placeholder="Quantity" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="totalPrice"> Total price: <span class="text-danger">*</span> </label>
                      <input type="text" id="totalPrice" name="totalPrice" class="form-control" placeholder="Total price" required>
                    </div>
                  </div>
                  <!-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="updatedAt"> Updated at: </label>
                      <input type="text" id="updatedAt" name="updatedAt" class="form-control" placeholder="Updated at">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="createdAt"> Created at: </label>
                      <input type="text" id="createdAt" name="createdAt" class="form-control" placeholder="Created at">
                    </div>
                  </div> -->
                </div>
                <div class="row">
                </div>

                <div class="form-group text-center">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success" id="edit-form-btn">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </form>

            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- modal logout -->
    <?= View("components/modal-logout") ?>
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.3
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="<?= base_url() ?>/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- jquery-validation -->
  <script src="<?= base_url() ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?= base_url() ?>/plugins/jquery-validation/additional-methods.min.js"></script>
  <!-- DataTables -->
  <script src="<?= base_url() ?>/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="<?= base_url() ?>/plugins/chart.js/Chart.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url() ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url() ?>/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
    $(function() {
      $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
          "url": '<?php echo base_url($controller . '/getAll') ?>',
          "type": "POST",
          "dataType": "json",
          async: "true"
        }
      });
    });

    function add() {
      // reset the form 
      $("#add-form")[0].reset();
      $(".form-control").removeClass('is-invalid').removeClass('is-valid');
      $('#add-modal').modal('show');
      // submit the add from 
      $.validator.setDefaults({
        highlight: function(element) {
          $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function(element) {
          $(element).removeClass('is-invalid').addClass('is-valid');
        },
        errorElement: 'div ',
        errorClass: 'invalid-feedback',
        errorPlacement: function(error, element) {
          if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
          } else if ($(element).is('.select')) {
            element.next().after(error);
          } else if (element.hasClass('select2')) {
            //error.insertAfter(element);
            error.insertAfter(element.next());
          } else if (element.hasClass('selectpicker')) {
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        },

        submitHandler: function(form) {

          var form = $('#add-form');
          // remove the text-danger
          $(".text-danger").remove();

          $.ajax({
            url: '<?php echo base_url($controller . '/add') ?>',
            type: 'post',
            data: form.serialize(), // /converting the form data into array and sending it to server
            dataType: 'json',
            beforeSend: function() {
              $('#add-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
            },
            success: function(response) {
              if (response.success === true) {

                Swal.fire({
                  position: 'bottom-end',
                  icon: 'success',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                }).then(function() {
                  $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                  $('#add-modal').modal('hide');
                })

              } else {

                if (response.messages instanceof Object) {
                  $.each(response.messages, function(index, value) {
                    var id = $("#" + index);

                    id.closest('.form-control')
                      .removeClass('is-invalid')
                      .removeClass('is-valid')
                      .addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

                    id.after(value);

                  });
                } else {
                  Swal.fire({
                    position: 'bottom-end',
                    icon: 'error',
                    title: response.messages,
                    showConfirmButton: false,
                    timer: 1500
                  })

                }
              }
              $('#add-form-btn').html('Add');
            }
          });

          return false;
        }
      });
      $('#add-form').validate();
    }

    function edit(id) {
      $.ajax({
        url: '<?php echo base_url($controller . '/getOne') ?>',
        type: 'post',
        data: {
          id: id
        },
        dataType: 'json',
        success: function(response) {
          // reset the form 
          $("#edit-form")[0].reset();
          $(".form-control").removeClass('is-invalid').removeClass('is-valid');
          $('#edit-modal').modal('show');

          $("#edit-form #id").val(response.id);
          $("#edit-form #productId").val(response.product_id);
          $("#edit-form #responsable").val(response.responsable);
          $("#edit-form #quantity").val(response.quantity);
          $("#edit-form #totalPrice").val(response.total_price);
          $("#edit-form #updatedAt").val(response.updated_at);
          $("#edit-form #createdAt").val(response.created_at);

          // submit the edit from 
          $.validator.setDefaults({
            highlight: function(element) {
              $(element).addClass('is-invalid').removeClass('is-valid');
            },
            unhighlight: function(element) {
              $(element).removeClass('is-invalid').addClass('is-valid');
            },
            errorElement: 'div ',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else if ($(element).is('.select')) {
                element.next().after(error);
              } else if (element.hasClass('select2')) {
                //error.insertAfter(element);
                error.insertAfter(element.next());
              } else if (element.hasClass('selectpicker')) {
                error.insertAfter(element.next());
              } else {
                error.insertAfter(element);
              }
            },

            submitHandler: function(form) {
              var form = $('#edit-form');
              $(".text-danger").remove();
              $.ajax({
                url: '<?php echo base_url($controller . '/edit') ?>',
                type: 'post',
                data: form.serialize(),
                dataType: 'json',
                beforeSend: function() {
                  $('#edit-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
                },
                success: function(response) {

                  if (response.success === true) {

                    Swal.fire({
                      position: 'bottom-end',
                      icon: 'success',
                      title: response.messages,
                      showConfirmButton: false,
                      timer: 1500
                    }).then(function() {
                      $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                      $('#edit-modal').modal('hide');
                    })

                  } else {

                    if (response.messages instanceof Object) {
                      $.each(response.messages, function(index, value) {
                        var id = $("#" + index);

                        id.closest('.form-control')
                          .removeClass('is-invalid')
                          .removeClass('is-valid')
                          .addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

                        id.after(value);

                      });
                    } else {
                      Swal.fire({
                        position: 'bottom-end',
                        icon: 'error',
                        title: response.messages,
                        showConfirmButton: false,
                        timer: 1500
                      })

                    }
                  }
                  $('#edit-form-btn').html('Update');
                }
              });

              return false;
            }
          });
          $('#edit-form').validate();

        }
      });
    }

    function remove(id) {
      Swal.fire({
        title: 'Are you sure of the deleting process?',
        text: "You cannot back after confirmation",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel'
      }).then((result) => {

        if (result.value) {
          $.ajax({
            url: '<?php echo base_url($controller . '/remove') ?>',
            type: 'post',
            data: {
              id: id
            },
            dataType: 'json',
            success: function(response) {

              if (response.success === true) {
                Swal.fire({
                  position: 'bottom-end',
                  icon: 'success',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                }).then(function() {
                  $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                })
              } else {
                Swal.fire({
                  position: 'bottom-end',
                  icon: 'error',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                })


              }
            }
          });
        }
      })
    }
  </script>






  <script>
    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData = {
      labels: <?= json_encode($label_stats) ?>,
      datasets: [{
        data: <?= json_encode($value_stats) ?>,
        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
      }]
    }
    var donutOptions = {
      maintainAspectRatio: false,
      responsive: true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })
  </script>
</body>

</html>