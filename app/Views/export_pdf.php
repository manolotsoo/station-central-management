<html>

<head>
  <title>Chart.js and jsPDF</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
  <style id="compiled-css" type="text/css">
    button {
      /*from ww  w .  jav  a 2  s  .c  o m*/
      margin-top: 30px;
      padding: 10px 20px;
      border-radius: 0;
    }
  </style>
  <script type="text/javascript">
    window.onload = function() {
      var chart_data = {
        labels: <?= json_encode($label_stats) ?>,
        datasets: [{
          fillColor: "rgba(6, 118, 152, 0.71)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: <?= json_encode($value_stats) ?>
        }]
      }
      //original canvas
      var canvas = document.querySelector('#cool-canvas');
      var context = canvas.getContext('2d');
      new Chart(context).Line(chart_data);
      //hidden canvas
      var newCanvas = document.querySelector('#supercool-canvas');
      newContext = newCanvas.getContext('2d');
      var supercoolcanvas = new Chart(newContext).Line(chart_data);
      supercoolcanvas.defaults.global = {
        scaleFontSize: 600
      }
      //add event listener to button
      document.getElementById('download-pdf').addEventListener("click", downloadPDF);
      //donwload pdf from original canvas
      function downloadPDF() {
        var canvas = document.querySelector('#cool-canvas');
        var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.text(15, 15, "Cool Chart");
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150);
        doc.save('canvas.pdf');
      }

      document.getElementById('download-pdf2').addEventListener("click", downloadPDF2);

      function downloadPDF2() {
        var newCanvas = document.querySelector('#supercool-canvas');
        var newCanvasImg = newCanvas.toDataURL("image/jpeg", 1.0);
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.text(15, 15, "Super Cool Chart");
        doc.addImage(newCanvasImg, 'JPEG', 10, 10, 280, 150);

        doc.text(10, 10, 'Hello everybody');
        doc.text(10, 20, 'My name is');
        doc.text(10, 40, 'Contact me at');
        doc.text(10, 30, 'I have just created a simple pdf using jspdf');
        doc.save('new-canvas.pdf');
      }
    }
  </script>
</head>

<body>
  <div>
    <canvas id="cool-canvas" width="600" height="300"></canvas>
  </div>
  <div style="height:0; width:0; overflow:hidden;">
    <canvas id="supercool-canvas" width="1200" height="600"></canvas>
  </div>
  <button type="button" id="download-pdf"> Download PDF </button>
  <button type="button" id="download-pdf2"> Download Higher Quality PDF </button>
</body>

</html>