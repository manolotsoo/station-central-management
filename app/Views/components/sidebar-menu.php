<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
    <li class="nav-header">Menus</li>
    <li class="nav-item">
      <a href="<?= base_url() ?>/importations" class="nav-link">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Historique d'importation
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="<?= base_url() ?>/products" class="nav-link">
        <i class="nav-icon far fa-image"></i>
        <p>
          Produits
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="<?= base_url() ?>/sales" class="nav-link">
        <i class="nav-icon fas fa-columns"></i>
        <p>
          Ventes
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="<?= base_url() ?>/stocks" class="nav-link">
        <i class="nav-icon fas fa-box"></i>
        <p>
          Stocks
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->