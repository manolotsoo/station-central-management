<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">

<!-- Notifications Dropdown Menu -->
<li class="nav-item dropdown">
  <a class="nav-link" data-toggle="dropdown" href="#">
    <i class="far fa-user"></i>
  </a>
  <div class="dropdown-menu dropdown-menu-right">
    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
      <i class="fas fa-sign-out-alt"></i>
      se déconnecter
    </a>
  </div>
</li>
</ul>