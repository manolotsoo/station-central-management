<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ventes - imported</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">


    <!-- top navbar -->
    <?= View("components/navbar") ?>

    <!-- the left sidebar -->
    <?= View("components/main-sidebar-container", ['titleLocation' => $titleLocation]) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <?= View("components/content-header.php", ["breadCrumb" => $breadCrumb, "titleLocation" => $titleLocation]) ?>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-6 mt-2">
                  </div>
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-3">
                    <form action="<?= base_url() ?>/sales/import_csv" method="POST" enctype="multipart/form-data">
                      <input type="hidden" name="location" value="<?= $titleLocation ?>">
                      <div class="form-group">
                        <label for="exampleInputFile">Importer un fichier de vente journalier</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" name="file_to_upload" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choisir un fichier</label>
                          </div>
                          <!-- <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                          </div> -->
                        </div>
                      </div>
                      <button type="submit" name="import" class="btn btn-block btn-info"> <i class="fa fa-file"></i> Importer</button>
                    </form>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Product id</th>
                      <th>Quantity</th>
                      <th>Total price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($totalSales as $sale) { ?>
                      <tr>
                        <td><?= $sale['product_id'] ?></td>
                        <td><?= $sale['quantity'] ?></td>
                        <td><?= $sale['total_price'] ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-6 mt-2">
                  </div>
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-3">
                    <form action="<?= base_url() ?>/sales/csv/save" method="POST">
                      <?php for ($i = 0; $i < $row_limit-1; $i++) { ?>
                        <input type="hidden" name="product_id[<?= $i ?>]" value="<?= $totalSales[$i]['product_id'] ?>">
                        <input type="hidden" name="quantity[<?= $i ?>]" value="<?= $totalSales[$i]['quantity'] ?>">
                        <input type="hidden" name="total_price[<?= $i ?>]" value="<?= $totalSales[$i]['total_price'] ?>">
                      <?php } ?>
                      <input type="hidden" name="row_limit" value="<?= $row_limit-1 ?>">
                      <input type="hidden" name="file_name" value="<?= $filename ?>">
                      <button id="button-save" type="submit" name="import" class="btn btn-block btn-info"> <i class="fa fa-file"></i> Sauvegarder</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- modal logout -->
    <?= View("components/modal-logout") ?>
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.3
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="<?= base_url() ?>/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- jquery-validation -->
  <script src="<?= base_url() ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?= base_url() ?>/plugins/jquery-validation/additional-methods.min.js"></script>
  <!-- DataTables -->
  <script src="<?= base_url() ?>/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?= base_url() ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url() ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url() ?>/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
    $(function() {
      $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
          "url": '<?php echo base_url($controller . '/getAll') ?>',
          "type": "POST",
          "dataType": "json",
          async: "true"
        }
      });
    });

    function add() {
      // reset the form 
      $("#add-form")[0].reset();
      $(".form-control").removeClass('is-invalid').removeClass('is-valid');
      $('#add-modal').modal('show');
      // submit the add from 
      $.validator.setDefaults({
        highlight: function(element) {
          $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function(element) {
          $(element).removeClass('is-invalid').addClass('is-valid');
        },
        errorElement: 'div ',
        errorClass: 'invalid-feedback',
        errorPlacement: function(error, element) {
          if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
          } else if ($(element).is('.select')) {
            element.next().after(error);
          } else if (element.hasClass('select2')) {
            //error.insertAfter(element);
            error.insertAfter(element.next());
          } else if (element.hasClass('selectpicker')) {
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        },

        submitHandler: function(form) {

          var form = $('#add-form');
          // remove the text-danger
          $(".text-danger").remove();

          $.ajax({
            url: '<?php echo base_url($controller . '/add') ?>',
            type: 'post',
            data: form.serialize(), // /converting the form data into array and sending it to server
            dataType: 'json',
            beforeSend: function() {
              $('#add-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
            },
            success: function(response) {
              if (response.success === true) {

                Swal.fire({
                  position: 'bottom-end',
                  icon: 'success',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                }).then(function() {
                  $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                  $('#add-modal').modal('hide');
                })

              } else {

                if (response.messages instanceof Object) {
                  $.each(response.messages, function(index, value) {
                    var id = $("#" + index);

                    id.closest('.form-control')
                      .removeClass('is-invalid')
                      .removeClass('is-valid')
                      .addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

                    id.after(value);

                  });
                } else {
                  Swal.fire({
                    position: 'bottom-end',
                    icon: 'error',
                    title: response.messages,
                    showConfirmButton: false,
                    timer: 1500
                  })

                }
              }
              $('#add-form-btn').html('Add');
            }
          });

          return false;
        }
      });
      $('#add-form').validate();
    }


    function edit(id) {
      $.ajax({
        url: '<?php echo base_url($controller . '/getOne') ?>',
        type: 'post',
        data: {
          id: id
        },
        dataType: 'json',
        success: function(response) {
          // reset the form 
          $("#edit-form")[0].reset();
          $(".form-control").removeClass('is-invalid').removeClass('is-valid');
          $('#edit-modal').modal('show');

          $("#edit-form #id").val(response.id);
          $("#edit-form #productId").val(response.product_id);
          $("#edit-form #responsable").val(response.responsable);
          $("#edit-form #quantity").val(response.quantity);
          $("#edit-form #totalPrice").val(response.total_price);
          $("#edit-form #updatedAt").val(response.updated_at);
          $("#edit-form #createdAt").val(response.created_at);

          // submit the edit from 
          $.validator.setDefaults({
            highlight: function(element) {
              $(element).addClass('is-invalid').removeClass('is-valid');
            },
            unhighlight: function(element) {
              $(element).removeClass('is-invalid').addClass('is-valid');
            },
            errorElement: 'div ',
            errorClass: 'invalid-feedback',
            errorPlacement: function(error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else if ($(element).is('.select')) {
                element.next().after(error);
              } else if (element.hasClass('select2')) {
                //error.insertAfter(element);
                error.insertAfter(element.next());
              } else if (element.hasClass('selectpicker')) {
                error.insertAfter(element.next());
              } else {
                error.insertAfter(element);
              }
            },

            submitHandler: function(form) {
              var form = $('#edit-form');
              $(".text-danger").remove();
              $.ajax({
                url: '<?php echo base_url($controller . '/edit') ?>',
                type: 'post',
                data: form.serialize(),
                dataType: 'json',
                beforeSend: function() {
                  $('#edit-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
                },
                success: function(response) {

                  if (response.success === true) {

                    Swal.fire({
                      position: 'bottom-end',
                      icon: 'success',
                      title: response.messages,
                      showConfirmButton: false,
                      timer: 1500
                    }).then(function() {
                      $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                      $('#edit-modal').modal('hide');
                    })

                  } else {

                    if (response.messages instanceof Object) {
                      $.each(response.messages, function(index, value) {
                        var id = $("#" + index);

                        id.closest('.form-control')
                          .removeClass('is-invalid')
                          .removeClass('is-valid')
                          .addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

                        id.after(value);

                      });
                    } else {
                      Swal.fire({
                        position: 'bottom-end',
                        icon: 'error',
                        title: response.messages,
                        showConfirmButton: false,
                        timer: 1500
                      })

                    }
                  }
                  $('#edit-form-btn').html('Update');
                }
              });

              return false;
            }
          });
          $('#edit-form').validate();

        }
      });
    }

    function remove(id) {
      Swal.fire({
        title: 'Are you sure of the deleting process?',
        text: "You cannot back after confirmation",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel'
      }).then((result) => {

        if (result.value) {
          $.ajax({
            url: '<?php echo base_url($controller . '/remove') ?>',
            type: 'post',
            data: {
              id: id
            },
            dataType: 'json',
            success: function(response) {

              if (response.success === true) {
                Swal.fire({
                  position: 'bottom-end',
                  icon: 'success',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                }).then(function() {
                  $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                })
              } else {
                Swal.fire({
                  position: 'bottom-end',
                  icon: 'error',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                })


              }
            }
          });
        }
      })
    }
  </script>
</body>

</html>