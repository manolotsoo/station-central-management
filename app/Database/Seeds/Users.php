<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Users extends Seeder
{
  protected $tableName = 'users';

  public function run()
  {
    $data = [
      [
        'email' => 'manolotsoa@centralstation.com',
        'user_name' => 'manolotsoa-central',
        '_password' => password_hash('123456', PASSWORD_BCRYPT)
      ],
      [
        'email' => 'razafindrakoto@centralstation.com',
        'user_name' => 'razafindrakoto-central',
        '_password' => password_hash('123456', PASSWORD_BCRYPT)
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
