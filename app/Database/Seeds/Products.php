<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Products extends Seeder
{
  protected $tableName = 'products';

  public function run()
  {
    $data = [
      [
        '_name' => 'gazolaitra',
        'cost_price' => 3491,
        'selling_price' => 3679,
        'evaporation_percent' => 0.004,
      ], [
        '_name' => 'lasantsy',
        'cost_price' => 3891,
        'selling_price' => 4671,
        'evaporation_percent' => 0.0001,
      ], [
        '_name' => 'petroly',
        'cost_price' => 1789,
        'selling_price' => 2184,
        'evaporation_percent' => 0.009,
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
