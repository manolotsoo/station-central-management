<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Stations extends Seeder
{
  protected $tableName = 'stations';

  public function run()
  {
    $data = [
      [
        '_name' => 'station-1',
        'address' => 'bira',
      ], [
        '_name' => 'station-2',
        'address' => 'tamaga',
      ], [
        '_name' => 'station-3',
        'address' => 'nosybe',
      ],
    ];
    // Using Query Builder
    $this->db->table($this->tableName)->insertBatch($data);
  }
}
