<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Importations extends Migration
{
  protected $tableName = 'importations';

	public function up()
	{
    $this->forge->addField(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'filename' => [
        'type' => 'VARCHAR',
        'constraint' => '100'
      ],
      'station_id' => [
        'type' => 'VARCHAR',
        'constraint' => 100,
      ],
    ));
    $this->forge->addKey('id', TRUE);
    $this->forge->addField("updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
    $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
    $this->forge->createTable($this->tableName);
	}

	public function down()
	{
    $this->forge->dropTable($this->tableName);
	}
}
