<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Sales extends Migration
{
  protected $tableName = 'sales';

  public function up()
  {
    $this->forge->addField(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_id' => [
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE
      ],
      // 'responsable' => [
      //   'type' => 'INT',
      //   'constraint' => 4,
      //   'unsigned' => TRUE
      // ],
      'quantity' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE
      ],
      'total_price' => [
        'type' => 'DOUBLE',
        'unsigned' => TRUE
      ],
      'station_id' => [
        'type' => 'INT',
        'constraint' => 4,
        'unsigned' => TRUE
      ]
    ));
    $this->forge->addKey('id', TRUE);
    $this->forge->addField("updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
    $this->forge->addField("created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
    // $this->forge->addField('CONSTRAINT FOREIGN KEY (responsable) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE');
    $this->forge->addField('CONSTRAINT FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE RESTRICT ON UPDATE CASCADE');
    $this->forge->addField('CONSTRAINT FOREIGN KEY (station_id) REFERENCES stations(id) ON DELETE RESTRICT ON UPDATE CASCADE');
    $this->forge->createTable($this->tableName);

  }

  public function down()
  {
    $this->forge->dropTable($this->tableName);
  }
}
