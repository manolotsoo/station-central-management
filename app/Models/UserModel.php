<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
  protected $table = 'users';
  protected $primaryKey = 'id';
  protected $returnType = 'array';
  protected $useSoftDeletes = false;
  protected $allowedFields = ['email', '_password', 'updated_at', 'created_at'];
  protected $useTimestamps = false;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';
  protected $deletedField  = 'deleted_at';
  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;
}
