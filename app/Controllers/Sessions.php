<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;

class Sessions extends BaseController
{
  public function index()
  {
    $data['titlePage'] = "Central Station";
    return view('sessions/index', $data);
  }

  public function new()
  {
    $data['titlePage'] = "Central Station";
    return view('sessions/new', $data);
  }

  public function login()
  {
    $request = service('request');
    $data = array(
      'email' => $request->getPost('email'),
      'password' => $request->getPost('mdp')
    );
    $userModel = new UserModel();
    $userToFind = $userModel->where('email', $data['email'])->findAll();
    if (password_verify($data['password'], $userToFind[0]['_password'])) {
      session()->set('connected', $userToFind[0]['id'] . "-" . $userToFind[0]['user_name']);
      return redirect()->to(base_url(["home"]));
    } else {
      session()->setFlashdata('user', array('message' => 'identifiant ou mot de passe incorrect', 'class' => 'danger'));
      return redirect()->to(base_url(["sessions", "new"]));
    }
  }

  public function logout()
  {
    session()->remove('connected');
    return redirect()->to(base_url(["sessions", "new"]));
  }
}
