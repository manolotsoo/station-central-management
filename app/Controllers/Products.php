<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\ProductModel;

class Products extends BaseController
{

  protected $productModel;
  protected $validation;

  public function __construct()
  {
    $this->productModel = new ProductModel();
    $this->validation =  \Config\Services::validation();
    helper(['users']);
  }

  public function index()
  {
    $data = [
      'controller'      => 'products',
      'title'         => 'products',
      'titleLocation' => "Station Central",
      'titlePage' => "Produits",
      'breadCrumb' => ["Home", "Produit"]
    ];

    return view('products', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->productModel->select('id, _name, cost_price, selling_price, evaporation_percent, updated_at, created_at')->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->_name,
        // $value->responsable,
        $value->cost_price,
        $value->selling_price,
        $value->evaporation_percent,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->productModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['_name'] = $this->request->getPost('name');
    // $fields['responsable'] = $this->request->getPost('responsable');
    $fields['cost_price'] = $this->request->getPost('costPrice');
    $fields['selling_price'] = $this->request->getPost('sellingPrice');
    $fields['evaporation_percent'] = $this->request->getPost('evaporationPercent');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      '_name' => ['label' => ' name', 'rules' => 'required|max_length[100]'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'cost_price' => ['label' => 'Cost price', 'rules' => 'required'],
      'selling_price' => ['label' => 'Selling price', 'rules' => 'required'],
      // 'evaporation_percent' => ['label' => 'Evaporation percent', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->productModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['_name'] = $this->request->getPost('name');
    $fields['responsable'] = $this->request->getPost('responsable');
    $fields['cost_price'] = $this->request->getPost('costPrice');
    $fields['selling_price'] = $this->request->getPost('sellingPrice');
    $fields['evaporation_percent'] = $this->request->getPost('evaporationPercent');
    $fields['updated_at'] = $this->request->getPost('updatedAt');
    $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      '_name' => ['label' => ' name', 'rules' => 'required|max_length[100]'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'cost_price' => ['label' => 'Cost price', 'rules' => 'required'],
      'selling_price' => ['label' => 'Selling price', 'rules' => 'required'],
      'evaporation_percent' => ['label' => 'Evaporation percent', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->productModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->productModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function export_csv()
  {
    header("Content-Type: text/csv; charset=utf-8");
    header("Content-disposition: attachment; filename=products-" . date('m-Y-d') . ".csv");

    $out = fopen('php://output', 'w');
    $products = new ProductModel();

    $productList = $products->findAll();
    foreach ($productList as $sale) {
      fputcsv($out, array(
        $sale->id,
        $sale->_name,
        $sale->cost_price,
        $sale->selling_price,
        $sale->evaporation_percent
      ));
    }
    fclose($out);
  }
}
