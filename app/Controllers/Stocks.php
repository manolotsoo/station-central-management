<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\StockModel;
use App\Models\ImportationModel;

class Stocks extends BaseController
{

  protected $stocksModel;
  protected $validation;

  public function __construct()
  {
    $this->stocksModel = new StockModel();
    $this->validation =  \Config\Services::validation();
    helper(['users', 'products', 'stocks']);
  }

  public function index()
  {

    $data = [
      'controller'      => 'stocks',
      'title'         => 'Stocks',
      'titleLocation' => "Station Central",
      'titlePage' => "Mouvement de Stocks",
      'breadCrumb' => ["Home", "Stock"],
      'products' => allProduct()
    ];
    $data['state_stocks'] = get_state_stocks();
    return view('stocks', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->stocksModel->select('id, product_id, quantity_entry, quantity_out, updated_at, created_at')
      // ->join('products','stocks.product_id=products.id')
      ->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->product_id,
        $value->quantity_entry,
        $value->quantity_out,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->stocksModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    $fields['quantity_entry'] = $this->request->getPost('quantityEntry');
    $fields['quantity_out'] = $this->request->getPost('quantityOut');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      'quantity_entry' => ['label' => 'Quantity entry', 'rules' => 'permit_empty'],
      'quantity_out' => ['label' => 'Quantity out', 'rules' => 'permit_empty'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->stocksModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    $fields['quantity_entry'] = $this->request->getPost('quantityEntry');
    $fields['quantity_out'] = $this->request->getPost('quantityOut');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      'quantity_entry' => ['label' => 'Quantity entry', 'rules' => 'permit_empty'],
      'quantity_out' => ['label' => 'Quantity out', 'rules' => 'permit_empty'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->stocksModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->stocksModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function import_csv()
  {
    $row = 1;
    $totalStocks = array();

    $file_to_upload_name = $_FILES['file_to_upload']['name'];
    $nameOfStation = explode("-", $file_to_upload_name);
    $location = implode("-", [$nameOfStation[1], $nameOfStation[2]]);

    if (($handle = fopen($_FILES['file_to_upload']['tmp_name'], "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        array_push($totalStocks, array(
          'product_id' => $data[0],
          'quantity_entry' => $data[1],
        ));
      }
      fclose($handle);
    }
    // $location = $this->request->getPost('location');
    return View('stocks_importation_view', [
      'totalStocks' => $totalStocks,
      'controller'      => 'stocks',
      'title'         => 'Stocks',
      'titleLocation' => $location,
      'filename' => $file_to_upload_name,
      'titlePage' => "stocks",
      'breadCrumb' => ["Home", "Stock", "aperçu"],
      'row_limit' => $row
    ]);
  }

  public function save()
  {
    $row_limit = $this->request->getPost('row_limit');
    $file_name = $this->request->getPost('file_name');
    $station_id = explode(".", explode("-", $file_name)[2])[0]  ;
    $stocks = array();
    for ($i = 0; $i < $row_limit; $i++) {
      $stocks['product_id'] = $this->request->getPost('product_id[' . $i . ']');
      // $sales['responsable'] = $this->request->getPost('responsable[' . $i . ']');
      $stocks['quantity_entry'] = $this->request->getPost('quantity[' . $i . ']');
      // $sales['total_price'] = $this->request->getPost('total_price[' . $i . ']');
      $stocks['station_id'] = $station_id;

      $this->validation->setRules([
        'product_id' => ['label' => 'Product id', 'rules' => 'numeric|permit_empty'],
        // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
        'quantity_entry' => ['label' => 'Quantity', 'rules' => 'required'],
        // 'total_price' => ['label' => 'Total price', 'rules' => 'required'],
        // 'station_id' => ['label' => 'Station', 'rules' => 'required']
      ]);
      if ($this->validation->run($stocks) == FALSE) {
        $response['success'] = false;
        $response['messages'] = $this->validation->listErrors();
        print_r($response['messages']);
        return false;
      } else {
        if ($this->stocksModel->insert($stocks)) {
          $response['success'] = true;
          $response['messages'] = 'Data has been inserted successfully';
          print_r($stocks);
        } else {
          $response['success'] = false;
          $response['messages'] = 'Insertion error!';
          print_r($stocks);

          return false;
        }
      }
    }

    $importation = new ImportationModel();

    $this->validation->setRules([
      'filename' => ['label' => 'Filename', 'rules' => 'required|max_length[100]|is_unique[importations.filename]'],
      'station_id' => ['label' => 'Station id', 'rules' => 'required|max_length[100]']
    ]);


    $fields['station_id'] = $station_id;
    $fields['filename'] = $file_name;

    if ($this->validation->run($fields) == FALSE) {
      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
      return false;
    } else {
      // insertion dans historique
      if ($importation->insert($fields)) {
        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
        return redirect()->to(base_url(["home", "index"]));
      } else {
        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
        echo "error";
      }
    }
  }
}
