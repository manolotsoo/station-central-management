<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ImportationModel;
use App\Models\ProductModel;
use App\Models\SaleModel;
use App\Models\StockModel;
use App\Models\StationModel;
use Config\View;
use \Config\Database;


class Sales extends BaseController
{

  protected $saleModel;
  protected $validation;

  public function __construct()
  {
    $this->saleModel = new SaleModel();
    $this->stockModel = new StockModel();
    $this->validation =  \Config\Services::validation();
    helper(['users', 'products', 'date', 'sales']);
  }

  public function index()
  {
    $productModel = new ProductModel();

    $data = [
      'controller'      => 'sales',
      'title'         => 'Sales',
      'titleLocation' => "Station Central",
      'titlePage' => "ventes",
      'breadCrumb' => ["Home", "Vente"],
      'products' => allProduct()
    ];
    $data['label_stats'] = get_label_per_product();
    $data['value_stats'] = get_value_per_product();
    $data['benefits'] = get_benefit();
    $data['stations_stats'] = get_sales_per_station();

    $benefits_total = 0;
    $sales_total = 0;
    foreach ($data['benefits'] as $b) {
      $benefits_total += $b['benefit'];
    }
    foreach ($data['stations_stats'] as $amount) {
      $sales_total += $amount['sales_per_station'];
    }
    $data['benefit_total'] = $benefits_total;
    $data['sale_total'] = $sales_total;
    return view('sales', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->saleModel->select('id, product_id, quantity, total_price, updated_at, created_at')->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->product_id,
        // $value->responsable,
        $value->quantity,
        $value->total_price,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->saleModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {
      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    // $fields['responsable'] = getIdConnected();
    $sales_type = $this->request->getPost('sales_type');
    $product = new ProductModel();
    $current_product = $product->find($fields['product_id']);
    if ($sales_type > 0) {
      // si montant 
      $fields['total_price'] = $this->request->getPost('totalPrice');
      $fields['quantity'] = $fields['total_price'] / $current_product->selling_price;
    } else {
      // si quantity
      $fields['quantity'] = $this->request->getPost('quantity');
      $fields['total_price'] = $fields['quantity'] * $current_product->selling_price;
    }


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'quantity' => ['label' => 'Quantity', 'rules' => 'required'],
      'total_price' => ['label' => 'Total price', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->saleModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['product_id'] = $this->request->getPost('productId');
    // $fields['responsable'] = $this->request->getPost('responsable');
    $fields['quantity'] = $this->request->getPost('quantity');
    $fields['total_price'] = $this->request->getPost('totalPrice');
    // $fields['updated_at'] = $this->request->getPost('updatedAt');
    // $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
      // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
      'quantity' => ['label' => 'Quantity', 'rules' => 'required'],
      'total_price' => ['label' => 'Total price', 'rules' => 'required'],
      // 'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      // 'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->saleModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->saleModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function import_csv()
  {
    $row = 1;
    $totalSales = array();

    $file_to_upload_name = $_FILES['file_to_upload']['name'];
    $nameOfStation = explode("-", $file_to_upload_name);
    $location = implode("-", [$nameOfStation[0], $nameOfStation[1]]);

    if (($handle = fopen($_FILES['file_to_upload']['tmp_name'], "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        array_push($totalSales, array(
          'product_id' => $data[1],
          'quantity' => $data[2],
          'total_price' => $data[3]
        ));
      }
      fclose($handle);
    }
    // $location = $this->request->getPost('location');
    return View('sales_importation_view', [
      'totalSales' => $totalSales,
      'controller'      => 'sales',
      'title'         => 'Sales',
      'titleLocation' => $location,
      'filename' => $file_to_upload_name,
      'titlePage' => "ventes",
      'breadCrumb' => ["Home", "Vente", "aperçu"],
      'row_limit' => $row
    ]);
  }

  public function save()
  {
    $row_limit = $this->request->getPost('row_limit');
    $file_name = $this->request->getPost('file_name');
    $station_id = explode("-", $file_name)[1];
    $sales = array();
    for ($i = 0; $i < $row_limit; $i++) {
      $sales['product_id'] = $this->request->getPost('product_id[' . $i . ']');
      // $sales['responsable'] = $this->request->getPost('responsable[' . $i . ']');
      $sales['quantity'] = $this->request->getPost('quantity[' . $i . ']');
      $sales['total_price'] = $this->request->getPost('total_price[' . $i . ']');
      $sales['station_id'] = $station_id;


      $this->validation->setRules([
        'product_id' => ['label' => 'Product id', 'rules' => 'required|numeric'],
        // 'responsable' => ['label' => 'Responsable', 'rules' => 'required|numeric'],
        'quantity' => ['label' => 'Quantity', 'rules' => 'required'],
        'total_price' => ['label' => 'Total price', 'rules' => 'required'],
        'station_id' => ['label' => 'Station', 'rules' => 'required']
      ]);
      if ($this->validation->run($sales) == FALSE) {
        $response['success'] = false;
        $response['messages'] = $this->validation->listErrors();
        print_r($response['messages']);
        return false;
      } else {
        if ($this->saleModel->insert($sales)) {
          $response['success'] = true;
          $response['messages'] = 'Data has been inserted successfully';
          print_r($sales);
        } else {
          $response['success'] = false;
          $response['messages'] = 'Insertion error!';
          print_r($sales);

          return false;
        }
      }
    }

    $importation = new ImportationModel();

    $this->validation->setRules([
      'filename' => ['label' => 'Filename', 'rules' => 'required|max_length[100]|is_unique[importations.filename]'],
      'station_id' => ['label' => 'Station id', 'rules' => 'required|max_length[100]']
    ]);


    $fields['station_id'] = $station_id;
    $fields['filename'] = $file_name;

    if ($this->validation->run($fields) == FALSE) {
      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
      return false;
    } else {
      // insertion dans historique
      if ($importation->insert($fields)) {
        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
        return redirect()->to(base_url(["home", "index"]));
      } else {
        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
        echo "error";
      }
    }

    // stocks
    $stock = new StockModel();
  }
  // details vente station 1
  function detailsale($station_id)
  {
    $data['station'] = "Station-2";

    $data['titlePage'] = "Liste Vente";
    $data['titleLocation'] = "Station";

    $data['sales'] = sales_per_stations_per_date($station_id);
    $data['station_id'] = $station_id;
    return View('view_sale', $data);
  }

  function export_pdf()
  {
    $data['label_stats'] = get_label_per_product();
    $data['value_stats'] = get_value_per_product();
    $data['benefits'] = get_benefit();
    $data['stations_stats'] = get_sales_per_station();

    $benefits_total = 0;
    $sales_total = 0;
    foreach ($data['benefits'] as $b) {
      $benefits_total += $b['benefit'];
    }
    foreach ($data['stations_stats'] as $amount) {
      $sales_total += $amount['sales_per_station'];
    }
    $data['benefit_total'] = $benefits_total;
    $data['sale_total'] = $sales_total;
    return View('export_pdf', $data);
  }

  function detail_per_date($station_id,$date)
  {
    $data['sales_details'] = sales_per_date_per_station($station_id,$date);
    return View('sales_per_date_per_stations', $data);
  }
}
