<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\ImportationModel;

class Importations extends BaseController
{

  protected $importationModel;
  protected $validation;

  public function __construct()
  {
    $this->importationModel = new ImportationModel();
    $this->validation =  \Config\Services::validation();
    helper(['users']);
  }

  public function index()
  {

    $data = [
      'controller'      => 'importations',
      'title'         => 'Importations',
      'titlePage' => 'Historique d\'Importations',
      'titleLocation' => 'Station central',
      'breadCrumb' => ["Home", "Importation"],
    ];

    return view('importation', $data);
  }

  public function getAll()
  {
    $response = array();

    $data['data'] = array();

    $result = $this->importationModel->select('id, filename, station_id, updated_at, created_at')->findAll();

    foreach ($result as $key => $value) {

      $ops = '<div class="btn-group">';
      $ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id . ')"><i class="fa fa-edit"></i></button>';
      $ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id . ')"><i class="fa fa-trash"></i></button>';
      $ops .= '</div>';

      $data['data'][$key] = array(
        $value->id,
        $value->filename,
        $value->station_id,
        $value->updated_at,
        $value->created_at,

        $ops,
      );
    }

    return $this->response->setJSON($data);
  }

  public function getOne()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if ($this->validation->check($id, 'required|numeric')) {

      $data = $this->importationModel->where('id', $id)->first();

      return $this->response->setJSON($data);
    } else {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    }
  }

  public function add()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['filename'] = $this->request->getPost('filename');
    $fields['station_id'] = $this->request->getPost('stationId');
    $fields['updated_at'] = $this->request->getPost('updatedAt');
    $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'filename' => ['label' => 'Filename', 'rules' => 'required|max_length[100]'],
      'station_id' => ['label' => 'Station id', 'rules' => 'required|max_length[100]'],
      'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->importationModel->insert($fields)) {

        $response['success'] = true;
        $response['messages'] = 'Data has been inserted successfully';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Insertion error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function edit()
  {

    $response = array();

    $fields['id'] = $this->request->getPost('id');
    $fields['filename'] = $this->request->getPost('filename');
    $fields['station_id'] = $this->request->getPost('stationId');
    $fields['updated_at'] = $this->request->getPost('updatedAt');
    $fields['created_at'] = $this->request->getPost('createdAt');


    $this->validation->setRules([
      'filename' => ['label' => 'Filename', 'rules' => 'required|max_length[100]'],
      'station_id' => ['label' => 'Station id', 'rules' => 'required|max_length[100]'],
      'updated_at' => ['label' => 'Updated at', 'rules' => 'permit_empty'],
      'created_at' => ['label' => 'Created at', 'rules' => 'permit_empty'],

    ]);

    if ($this->validation->run($fields) == FALSE) {

      $response['success'] = false;
      $response['messages'] = $this->validation->listErrors();
    } else {

      if ($this->importationModel->update($fields['id'], $fields)) {

        $response['success'] = true;
        $response['messages'] = 'Successfully updated';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Update error!';
      }
    }

    return $this->response->setJSON($response);
  }

  public function remove()
  {
    $response = array();

    $id = $this->request->getPost('id');

    if (!$this->validation->check($id, 'required|numeric')) {

      throw new \CodeIgniter\Exceptions\PageNotFoundException();
    } else {

      if ($this->importationModel->where('id', $id)->delete()) {

        $response['success'] = true;
        $response['messages'] = 'Deletion succeeded';
      } else {

        $response['success'] = false;
        $response['messages'] = 'Deletion error!';
      }
    }

    return $this->response->setJSON($response);
  }
}
