<?php

namespace App\Controllers;

use App\Models\StationModel;

class Home extends BaseController
{

  public function __construct()
  {
    helper(['users','sales']);
  }


  public function index()
  {
    if (!getIdConnected()) {
      return redirect()->to(base_url(["sessions", "new"]));
    }
    $data["titleLocation"] = "Central Station";
    $data["titlePage"] = "Etat";
    $data["breadCrumb"] = ["Home",""];
    $stationModel = new StationModel();
    $data['stations'] = sales_group_by_stations();
    // $data['station1'] = sales_per_station(1);
    // $data['station2'] = sales_per_station(2);
    // $data['station3'] = sales_per_station(3);
    // print_r($data['station1']);
    // print_r($data['stations']);
    return view('layouts/index', $data);
  }
}
